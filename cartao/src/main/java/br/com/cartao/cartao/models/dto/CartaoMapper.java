package br.com.cartao.cartao.models.dto;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(createCartaoRequest.getNumber());

        Cliente cliente = new Cliente();
        cliente.setId(createCartaoRequest.getCustomerId());
        cartao.setClienteId(cliente.getId());

        return cartao;
    }

    public CreateCartaoResponse toCreateCartaoResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumber(cartao.getNumero());
        createCartaoResponse.setCustomerId(cartao.getClienteId());
        createCartaoResponse.setActive(cartao.getAtivo());

        return createCartaoResponse;
    }

    public Cartao toCartao(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();

        cartao.setNumero(updateCartaoRequest.getNumber());
        cartao.setAtivo(updateCartaoRequest.getActive());

        return cartao;
    }

    public UpdateCartaoResponse toUpdateCartaoResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumber(cartao.getNumero());
        updateCartaoResponse.setCustomerId(cartao.getClienteId());
        updateCartaoResponse.setActive(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public GetCartaoResponse toGetCartaoResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumber(cartao.getNumero());
        getCartaoResponse.setCustomerId(cartao.getClienteId());

        return getCartaoResponse;
    }

}
