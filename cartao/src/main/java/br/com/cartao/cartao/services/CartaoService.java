package br.com.cartao.cartao.services;

import br.com.cartao.cartao.clients.ClienteClient;
import br.com.cartao.cartao.exceptions.CartaoNotFoundException;
import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.Cliente;
import br.com.cartao.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao criar(Cartao cartao){
        Cliente cliente = clienteClient.findById(cartao.getClienteId());

        cartao.setClienteId(cliente.getId());
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao atualizar(Cartao cartao){

        Cartao cartaoRetorno = getByNumber(cartao.getNumero());

        cartaoRetorno.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(cartaoRetorno);

    }

    public Cartao getByNumber(String number) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(number);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getById(Integer id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }



}
