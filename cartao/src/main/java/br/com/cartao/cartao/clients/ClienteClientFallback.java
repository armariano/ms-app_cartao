package br.com.cartao.cartao.clients;

import br.com.cartao.cartao.models.Cliente;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente findById(Integer id) {
        Cliente cliente = new Cliente();
        cliente.setId(100);
        cliente.setNome("Cliente Fallback");
        return cliente;
    }
}
