package br.com.cartao.cartao.controllers;

import br.com.cartao.cartao.exceptions.CartaoNotFoundException;
import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.dto.*;
import br.com.cartao.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")

public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper mapper;

    @PostMapping
    public ResponseEntity<?> criar(@Valid @RequestBody CreateCartaoRequest createCartaoRequest) {

        /*  O CODIGO FOI COMENTADO PARA A EXCEPTION DO CLIENT FUNCIONAR*/
        /*try {*/
            Cartao cartao = mapper.toCartao(createCartaoRequest);
            cartao = cartaoService.criar(cartao);

            return ResponseEntity.status(201).body(mapper.toCreateCartaoResponse(cartao));
        /*} catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }*/
    }

    @PatchMapping("/{number}")
    public ResponseEntity<?> update(@PathVariable String number, @RequestBody UpdateCartaoRequest updateCartaoRequest) {

        try{
            updateCartaoRequest.setNumber(number);
            Cartao cartao = mapper.toCartao(updateCartaoRequest);
            cartao = cartaoService.atualizar(cartao);

            return ResponseEntity.status(200).body(mapper.toUpdateCartaoResponse(cartao));
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{number}")
    public ResponseEntity<?> getByNumber(@PathVariable String number) {

        try{
            Cartao cartao = cartaoService.getByNumber(number);

            return ResponseEntity.status(200).body(mapper.toGetCartaoResponse(cartao));
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {

        try{
            Cartao cartao = cartaoService.getById(id);

            return ResponseEntity.status(200).body(mapper.toGetCartaoResponse(cartao));
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
