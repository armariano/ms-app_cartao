package br.com.cartao.cartao.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateCartaoResponse {

    private Integer id;

    @JsonProperty("numero")
    private String number;

    @JsonProperty("clienteId")
    private Integer customerId;

    @JsonProperty("ativo")
    private Boolean active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
