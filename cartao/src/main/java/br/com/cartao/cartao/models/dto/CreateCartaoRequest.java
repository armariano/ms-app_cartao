package br.com.cartao.cartao.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class CreateCartaoRequest {

    @JsonProperty("numero")
    private String number;

    @NotNull
    @JsonProperty("clienteId")
    private Integer customerId;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

}
