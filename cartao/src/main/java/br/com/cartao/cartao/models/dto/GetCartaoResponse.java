package br.com.cartao.cartao.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCartaoResponse {

    private Integer id;

    @JsonProperty("numero")
    private String number;

    @JsonProperty("clienteId")
    private Integer customerId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

}
