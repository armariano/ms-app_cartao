package br.com.pagamento.pagamento.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cartão informado é inválido!")
public class InvalidCartaoException extends RuntimeException {
}
