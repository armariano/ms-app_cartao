package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", url = "http://localhost:8081/cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/id/{id}")
    Cartao findById(@PathVariable Integer id);
}
