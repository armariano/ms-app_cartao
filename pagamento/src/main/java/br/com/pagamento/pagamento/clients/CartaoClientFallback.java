package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.models.Cartao;

public class CartaoClientFallback implements CartaoClient {

    @Override
    public Cartao findById(Integer id) {

        //Tratamento exemplo de fallback
        Cartao cartao = new Cartao();
        cartao.setId(100);
        cartao.setNumero("999999999");
        cartao.setAtivo(false);
        cartao.setClienteId(100);

        return cartao;
    }
}
