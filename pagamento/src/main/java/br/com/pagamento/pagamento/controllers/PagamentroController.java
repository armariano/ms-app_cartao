package br.com.pagamento.pagamento.controllers;

import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentroController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<?> criar(@RequestBody Pagamento pagamento) {
        return ResponseEntity.status(201).body(pagamentoService.create(pagamento));
    }

    @GetMapping("/{cartaoId}")
    public ResponseEntity<?>  findAllByCartaoId(@PathVariable Integer cartaoId) {
        try {
            return ResponseEntity.status(200).body(pagamentoService.findAllByCartaoId(cartaoId));
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
