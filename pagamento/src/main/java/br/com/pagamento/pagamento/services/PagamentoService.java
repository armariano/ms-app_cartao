package br.com.pagamento.pagamento.services;

import br.com.pagamento.pagamento.clients.CartaoClient;
import br.com.pagamento.pagamento.models.Cartao;
import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento create(Pagamento pagamento) {
        Cartao creditCard = cartaoClient.findById(pagamento.getCartaoId());

        pagamento.setCartaoId(creditCard.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> findAllByCartaoId(Integer cartaoId) {
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }

}
