package br.com.pagamento.pagamento.repositories;

import br.com.pagamento.pagamento.models.Cartao;
import br.com.pagamento.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    List<Pagamento> findAllByCartaoId(Integer id);
}
